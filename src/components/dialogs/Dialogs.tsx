// @flow
import {inject, observer} from "mobx-react";
import React from "react";
import Login from "./Login";
import {AppStoreProps} from "../../store/AppStore";

const Dialogs = observer((props: AppStoreProps) => {
    if (!props.app)
        return null;
    const app = props.app;
    return <>
        <Login open={app.dialogLogin} onClose={() => app.dialogLogin = false}/>
    </>
});

export default inject('app')(Dialogs);
