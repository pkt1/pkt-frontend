import React, {useState} from "react";
import {Button, Dialog, DialogContent, DialogTitle, LinearProgress, Typography} from "@material-ui/core";
import {DialogProps} from "@material-ui/core/Dialog/Dialog";
import {RouteComponentProps, withRouter} from "react-router";
import DialogActions from "@material-ui/core/DialogActions";
import TextField from "@material-ui/core/TextField";
import {AppStoreProps} from "../../store/AppStore";
import {inject, observer} from "mobx-react";


const Login: React.FC<RouteComponentProps & DialogProps & AppStoreProps> = (props) => {

    const [login, setLogin] = useState("");
    const [pass, setPass] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");

    const close = () => {
        if (props.onClose)
            props.onClose({}, "backdropClick");
    }

    const app = props.app!!;

    return <Dialog {...props} fullWidth>
        {loading && <LinearProgress className={"fullWidth"}/>}
        <DialogTitle>
            Войти
        </DialogTitle>
        <DialogContent>
            <div className={"padded"}>
                <TextField variant={"outlined"}
                           value={login}
                           onChange={e => {
                               setError("");
                               setLogin(e.target.value);
                           }}
                           label={"Логин"}

                           fullWidth/>
            </div>
            <div className={"padded"}>
                <TextField variant={"outlined"}
                           value={pass}
                           onChange={e => {
                               setError("");
                               setPass(e.target.value)
                           }}
                           label={"Пароль"}
                           fullWidth
                           type={"password"}/>
            </div>
            {error && <Typography color={"error"} variant={"h6"}>
                {error}
            </Typography>}
        </DialogContent>
        <DialogActions>
            <Button color={"secondary"} variant={"contained"} onClick={close} disabled={loading}>
                Отмена
            </Button>
            <Button color={"primary"} variant={"contained"} disabled={loading} onClick={() => {
                setLoading(true);
                app.api.register(login, pass)
                    .then(() => app.api.login(login, pass))
                    .then(resp => {
                        app.api.setToken(resp.token);
                        app.setUserName(login);
                        setLoading(false);
                        close()
                    })
                    .catch(e => {
                        setError("Ошибка регистрации");
                        console.error(e);
                        setLoading(false);
                    });
            }}>
                Зарегистрироваться
            </Button>
            <Button color={"primary"} variant={"contained"} disabled={loading} onClick={() => {
                setLoading(true);
                app.api.login(login, pass)
                    .then(resp => {
                        app.api.setToken(resp.token);
                        app.setUserName(login);
                        setLoading(false);
                        close()
                    })
                    .catch(e => {
                        setError("Неверный логин/пароль");
                        console.error(e);
                    });
            }}>
                Войти
            </Button>

        </DialogActions>
    </Dialog>
}

export default withRouter(inject('app')(observer(Login)))
