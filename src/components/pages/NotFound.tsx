import React from "react";
import {Typography} from "@material-ui/core";

const NotFound: React.FC = () => {
    return <div
        style={{display: "flex", width: "100%", height: "100vh", alignItems: "center", justifyContent: "center"}}>
        <Typography variant={"h3"}>
            404 - не найдено
        </Typography>
    </div>
};

export default NotFound;
