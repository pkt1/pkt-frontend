import React, {Component} from "react";
import {Track} from "../../tools/Types";
import {observable} from "mobx";
import {RouteComponentProps, withRouter} from "react-router";
import {inject, observer} from "mobx-react";
import {AppStoreProps} from "../../store/AppStore";
import Typography from "@material-ui/core/Typography";
import TrackMap from "../cards/TrackMap";
import CommentCard from "../cards/CommentCard";
import {Button} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

class TrackPage extends Component<RouteComponentProps<{ id: string }> & AppStoreProps> {
    @observable private descr = "qwe";
    @observable private track: Track | null = null;
    @observable private mycomment = "";

    constructor(props) {
        super(props);
        this.updateTrack = this.updateTrack.bind(this);
    }

    updateTrack() {
        const id = this.props.match.params.id;
        this.props.app!!.api.getTrack(id).then(track => {
            this.track = track;
        }).catch(e => {
            console.error(e);
        });
    }

    componentDidMount() {
        this.updateTrack()
    }

    render() {
        const track = this.track;
        if (!track)
            return null;
        const app = this.props.app!!;
        // @ts-ignore
        return <div style={{width: "100%", display: "flex", justifyContent: "center"}}>
            <div style={{display: "flex", alignItems: "center", flexDirection: "column", width: 600}}>
                <div style={{padding: 30, width: "100%"}}>
                    <div style={{display: "flex", alignItems: "center", flexDirection: "row", textAlign: "center"}}>
                        <div style={{flex: 1}}>
                            <Typography variant={"h5"}>
                                By: {track.owner.login}
                            </Typography>
                        </div>
                        <div style={{flex: 1}}>
                            <Typography variant={"h4"}>
                                {track.name}
                            </Typography>
                        </div>
                        <div style={{flex: 1}}>
                            <Typography variant={"h5"}>
                                {track.nearestTown}
                            </Typography>
                        </div>
                    </div>
                </div>
                <TrackMap track={track}/>
                <div style={{width: "100%"}}>
                    <div className={"padded"}>
                        <Typography variant={"h6"}>
                            Описание: {track.description}
                        </Typography>
                    </div>
                </div>
                <div style={{width: "100%"}}>
                    <div className={"padded"}>
                        <Typography variant={"h5"}>
                            Комментарии ({track.comments.length})
                        </Typography>
                    </div>
                </div>
                <div style={{width: "100%", display: "flex", justifyContent: "center"}}>
                    <TextField multiline variant={"outlined"} value={this.mycomment}
                               onChange={e => this.mycomment = e.target.value} fullWidth/>
                    <div className={"padded"}>
                        <Button color={"primary"} variant={"contained"} onClick={() => {
                            app.api.addComment(track.id, this.mycomment).then(() => {
                                this.updateTrack()
                            }).catch(e => {
                                console.error(e);
                            })
                        }}>
                            Добавить
                        </Button>
                    </div>
                </div>
                {track.comments.map(comment => <div key={comment.text} style={{width: "100%"}} className={"padded"}>
                    <CommentCard comment={comment}/>
                </div>)}
            </div>
        </div>;
    }
}

export default withRouter(inject('app')(observer(TrackPage)))
