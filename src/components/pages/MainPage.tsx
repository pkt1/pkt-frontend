import React from 'react';
import TextField from "@material-ui/core/TextField";
import {Paper} from "@material-ui/core";
import {observable} from "mobx";
import {RouteComponentProps, withRouter} from "react-router";
import {inject, observer} from "mobx-react";
import {AppStoreProps} from "../../store/AppStore";
import {Track} from "../../tools/Types";
import TrackCard from "../cards/TrackCard";
import Button from "@material-ui/core/Button";

class MainPage extends React.Component<RouteComponentProps & AppStoreProps> {
    @observable private search = "";
    @observable private searchMode = "towns" as "towns" | "tracks";
    @observable private tracks: Track[] = [];
    @observable private searchTracks: Track[] = [];

    constructor(props) {
        super(props);
        this.doSearch = this.doSearch.bind(this);
    }


    componentDidMount() {
        this.props.app!!.api.allTracks(1, 10).then(tracks => {
            this.tracks = tracks;
            // console.log(tracks)
        }).catch(e => {
            console.error(e);
        })
    }

    doSearch() {
        const app = this.props.app!!;
        this.searchTracks = [];
        if (this.searchMode === "towns") {
            app.api.byCity(1, 10, this.search).then(resp => {
                this.searchTracks = resp;
            }).catch(e => {
                console.error(e);
            })
        }
        if (this.searchMode === "tracks") {
            app.api.byName(1, 10, this.search).then(resp => {
                this.searchTracks = resp;
            }).catch(e => {
                console.error(e);
            })
        }
    }

    render() {
        return <div style={{width: "100%", display: "flex", justifyContent: "center"}}>
            <div style={{display: "flex", alignItems: "center", flexDirection: "column", width: 600}}>
                <div style={{padding: 24, width: "100%"}}>
                    <Paper className={"paper"}>
                        <div className={"padded"}>
                            <TextField value={this.search}
                                       onChange={e => {
                                           this.search = e.target.value
                                           this.doSearch()
                                       }}
                                       fullWidth
                                       size={"medium"} label={"Поиск"}/>
                        </div>
                    </Paper>
                </div>
                {!this.search && this.tracks.map(track => <div style={{padding: 24, width: "100%"}} key={track.id}>
                    <TrackCard track={track}/>
                </div>)}
                {this.search && <div style={{display: "flex", flexDirection: "row", width: "100%"}}>
                    <div className={"padded"} style={{flexGrow: 1}}>
                        <Button color={this.searchMode === "towns" ? "secondary" : "primary"}
                                className={"padded"} variant={"contained"} fullWidth
                                onClick={() => {
                                    this.searchMode = "towns";
                                    this.doSearch()
                                }}>
                            Города
                        </Button>
                    </div>
                    <div className={"padded"} style={{flexGrow: 1}}>
                        <Button color={this.searchMode === "tracks" ? "secondary" : "primary"}
                                className={"padded"} variant={"contained"} fullWidth
                                onClick={() => {
                                    this.searchMode = "tracks";
                                    this.doSearch()
                                }}>
                            Маршруты
                        </Button>
                    </div>
                </div>}
                {this.search && this.searchTracks.map(track => <div style={{padding: 24, width: "100%"}} key={track.id}>
                    <TrackCard track={track}/>
                </div>)}
            </div>
        </div>
    }
}

export default withRouter(inject('app')(observer(MainPage)))
