import React from 'react';
import {observable} from "mobx";
import {RouteComponentProps, withRouter} from "react-router";
import {AppStoreProps} from "../../store/AppStore";
import {Button, ButtonProps, TextField} from "@material-ui/core";
import {Track} from "../../tools/Types";
import Typography from "@material-ui/core/Typography";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import LinearProgress from "@material-ui/core/LinearProgress";
import {inject, observer} from "mobx-react";

type UploadButtonProps = {
    onSelected: (f: File) => unknown
} & ButtonProps;

const objectWithoutKey = (object, key) => {
    const {[key]: deletedKey, ...otherKeys} = object;
    return otherKeys;
}

class UploadButton extends React.Component<UploadButtonProps> {
    private input: HTMLInputElement | null = null;

    render() {
        const buttonProps = objectWithoutKey(this.props, "onSelected");
        return <>
            <input
                ref={instance => this.input = instance}
                style={{display: "none"}}
                id="contained-button-file"
                type="file"
                onChange={() => {
                    if (!this.input!!.files)
                        return;
                    this.props.onSelected(this.input!!.files[0])
                }}
            />
            <Button variant={"contained"} color={"primary"} {...buttonProps} onClick={() => this.input!!.click()}>
                Загрузить файл трека (формат .gpx)
            </Button>
        </>;
    }
}

class NewTrack extends React.Component<RouteComponentProps & AppStoreProps> {
    @observable private search = "";
    @observable private searchMode = "towns" as "towns" | "tracks";
    @observable private tracks = [] as Track[];
    @observable private step = 0;

    @observable private name = "";
    @observable private town = "";
    @observable private descr = "";

    @observable private id = -1;

    @observable private loading = false;

    componentDidMount() {
        if (this.props.app!!.userName === "")
            this.props.history.replace("/")
    }

    render() {
        const app = this.props.app!!;
        return <div style={{width: "100%", display: "flex", justifyContent: "center"}}>
            <div style={{display: "flex", alignItems: "center", flexDirection: "column", width: 600}}>
                <div style={{padding: 30}}>
                    <Typography variant={"h4"}>
                        Создание маршрута
                    </Typography>
                    <Stepper activeStep={this.step}>
                        <Step completed={this.step >= 1}>
                            <StepLabel>Информация</StepLabel>
                        </Step>
                        <Step completed={this.step >= 2}>
                            <StepLabel>Файл трека</StepLabel>
                        </Step>
                    </Stepper>
                    <LinearProgress style={{display: this.loading ? undefined : "none", width: "100%"}}/>
                </div>


                {this.step === 0 && <div style={{width: "100%"}}>
                    <div className={"padded"}>
                        <TextField value={this.name} label={"Название"} onChange={e => this.name = e.target.value}
                                   fullWidth/>
                    </div>
                    <div className={"padded"}>
                        <TextField value={this.town} label={"Город"} onChange={e => this.town = e.target.value}
                                   fullWidth/>
                    </div>
                    <div className={"padded"}>
                        <TextField value={this.descr} label={"Описание"} onChange={e => this.descr = e.target.value}
                                   multiline fullWidth/>
                    </div>
                    <div className={"padded"}>
                        <Button variant={"contained"} color={"primary"} fullWidth disabled={this.loading}
                                onClick={() => {
                                    this.loading = true;
                                    app.api.createTrack(this.name, this.town, this.descr).then((resp) => {
                                        this.id = resp.id;
                                        this.step++;
                                        this.loading = false;
                                    }).catch(e => {
                                        console.error(e);
                                        this.loading = false;
                                    })
                                }}>
                            Далее
                        </Button>
                    </div>
                </div>}


                {this.step === 1 && <div style={{
                    width: "100%",
                    height: "100%",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                    <UploadButton disabled={this.loading} onSelected={file => {
                        this.loading = true;
                        app.api.uploadCoordinates(this.id, file)
                            .then(() => {
                                this.loading = false;
                                this.props.history.push(`/track/${this.id}`);
                            })
                            .catch(e => {
                                console.error(e);
                                this.loading = false;
                            });
                    }}/>
                </div>}
            </div>
        </div>
    }
}

export default withRouter(inject('app')(observer(NewTrack)))
