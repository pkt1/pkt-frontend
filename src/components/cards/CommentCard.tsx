import React from "react";
import {RouteComponentProps, withRouter} from 'react-router';
import Typography from "@material-ui/core/Typography";
import {Comment} from "../../tools/Types";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import {inject, observer} from "mobx-react";
import {AppStoreProps} from "../../store/AppStore";

export type Props = {
    comment: Comment
}

class CommentCard extends React.Component<RouteComponentProps & Props & AppStoreProps> {
    render() {
        const comment = this.props.comment;

        return <Card>
            <CardContent>
                <Typography gutterBottom variant="h5">
                    {comment.author.login}
                </Typography>
                <Typography variant="body2" component="p">
                    {comment.text}
                </Typography>
            </CardContent>
        </Card>
    }
}

export default withRouter(inject('app')(observer(CommentCard)))
