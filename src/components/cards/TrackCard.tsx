import React from "react";
import Button from "@material-ui/core/Button";
import {RouteComponentProps, withRouter} from 'react-router';
import Typography from "@material-ui/core/Typography";
import {Track} from "../../tools/Types";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import {inject, observer} from "mobx-react";
import {AppStoreProps} from "../../store/AppStore";
import TrackMap from "./TrackMap";

export type Props = {
    track: Track
}

class TrackCard extends React.Component<RouteComponentProps & Props & AppStoreProps> {
    render() {
        const track = this.props.track;

        return <Card>
            <TrackMap track={track}/>
            <CardContent>
                <Typography gutterBottom variant="h5">
                    {track.name} by {track.owner.login}
                </Typography>
                <Typography variant="body2" component="p">
                    {track.description}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" color="primary" onClick={() => {
                    this.props.history.push(`/track/${track.id}`)
                }}>
                    Открыть
                </Button>
            </CardActions>
        </Card>
    }
}

export default withRouter(inject('app')(observer(TrackCard)))
