import {coordToMap, Track} from "../../tools/Types";
import {Map, Marker, Polyline, TileLayer, Tooltip} from "react-leaflet";
import React from "react";

export const TrackMap: React.FC<{ track: Track }> = ({track}) => {
    if (!track.coordinates || !track.coordinates.length)
        return null;
    return <Map style={{width: "100%", height: 300}} zoom={12}
                center={coordToMap(track.coordinates[0])}>
        <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={coordToMap(track.coordinates[0])}>
            <Tooltip permanent>
                <span>Начало</span>
            </Tooltip>
        </Marker>
        <Marker position={coordToMap(track.coordinates[track.coordinates.length - 1])}>
            <Tooltip permanent>
                <span>Конец</span>
            </Tooltip>
        </Marker>
        <Polyline positions={track.coordinates.map(coord => coordToMap(coord))}/>
    </Map>
}

export default TrackMap;
