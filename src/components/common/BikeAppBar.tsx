import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar"
import Button from "@material-ui/core/Button";
import {RouterProps, withRouter} from 'react-router';
import Typography from "@material-ui/core/Typography";
import {ButtonBase} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {inject, observer} from "mobx-react";
import {AppStoreProps} from "../../store/AppStore";

const useStyles = makeStyles(() => ({
    appBar: {
        justifyContent: "space-between",
    },
    box: {
        flex: 1,
        display: "flex",
        justifyContent: "center"
    },
    left: {
        justifyContent: "flex-start"
    },
    right: {
        justifyContent: "flex-end"
    }
}));

const BikeAppBar: React.FC<RouterProps & AppStoreProps> = (props) => {
    const classes = useStyles();

    const app = props.app!!;

    return <AppBar position="static">
        <Toolbar className={classes.appBar}>
            <div className={`${classes.box} ${classes.left}`}>
                <ButtonBase onClick={() => {
                    props.history.push("/")
                }} className={"padded"}>
                    <Typography variant="h6">
                        Bike-tracks.me
                    </Typography>
                </ButtonBase>
            </div>
            <div className={classes.box}>
                <Button color="secondary" variant={"contained"} onClick={() => {
                    app.userName ? props.history.push("/new") : app.dialogLogin = true
                }}>
                    Добавить маршрут
                </Button>
            </div>
            <div className={`${classes.box} ${classes.right}`}>
                <ButtonBase onClick={() => {
                    if (app.userName) {
                        localStorage.removeItem("token");
                        localStorage.removeItem("userName");
                        document.location = document.location;
                        return;
                    }
                    props.app!!.dialogLogin = true;
                }} className={"padded"}>
                    <Typography variant="h6">
                        {app.userName ? app.userName : "Войти"}
                    </Typography>
                </ButtonBase>
            </div>
        </Toolbar>
    </AppBar>
};

export default withRouter(inject('app')(observer(BikeAppBar)))
