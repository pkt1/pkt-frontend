import {action, observable} from 'mobx';
import {BikeApi} from "../api/Api";

export type AppStoreProps = {
    app?: AppStore
}

export class AppStore {
    @observable userName = localStorage.getItem("userName") || "";
    @observable dialogLogin = false;
    @observable api = new BikeApi(process.env.REACT_APP_API_URL + "");

    @action setUserName = (userName: string) => {
        this.userName = userName;
        localStorage.setItem("userName", userName)
    }
}
