export type Track = {
    id: number,
    name: string,
    nearestTown: string,
    creationDate: string,
    description: string,
    length: number,
    numberDays: number,
    numberRestDays: number,
    category: number,
    coordinates: Coordinate[],
    comments: Comment[],
    owner: User
}

export type Comment = {
    text: string,
    author: User
}

export type User = {
    login: string,
}

export type Coordinate = {
    id: number,
    x: number,
    y: number,
    height: number
}

export function coordToMap(c: Coordinate) {
    return {lat: c.x, lng: c.y, alt: c.height}
}
