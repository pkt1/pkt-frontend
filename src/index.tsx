import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Router} from "react-router";
import {createBrowserHistory} from "history";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {createMuiTheme, ThemeProvider} from "@material-ui/core";
import {blue, cyan} from "@material-ui/core/colors";
import {Provider} from "mobx-react";
import {AppStore} from "./store/AppStore";

import {config} from "dotenv";

config()

const useStyles = makeStyles((theme) => ({
    "@global": {
        ".padded": {
            padding: theme.spacing(1)
        },
        ".dialog": {
            minWidth: 400
        },
        ".paper": {
            backgroundColor: "#d1faff"
        }
    }
}));

const theme = createMuiTheme({
    palette: {
        primary: blue,
        secondary: cyan,
    }
});

const history = createBrowserHistory();

const stores = {
    app: new AppStore()
};

const Index: React.FC<any> = () => {
    useStyles();
    return <ThemeProvider theme={theme}>
        <Provider {...stores}>
            <Router history={history}> <App/></Router>
        </Provider>
    </ThemeProvider>;
}

ReactDOM.render(
    <Index/>,
    document.getElementById('root')
);
