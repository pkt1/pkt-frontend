import React from 'react';
import {Route, Switch} from "react-router-dom";
import BikeAppBar from "./components/common/BikeAppBar";
import MainPage from "./components/pages/MainPage";
import Dialogs from "./components/dialogs/Dialogs";
import NotFound from "./components/pages/NotFound";
import NewTrack from "./components/pages/NewTrack";
import TrackPage from "./components/pages/TrackPage";
import {inject, observer} from "mobx-react";
import {AppStoreProps} from "./store/AppStore";


class App extends React.Component<AppStoreProps> {

    render() {
        return <div>
            <BikeAppBar/>
            <Dialogs/>
            <Switch>
                <Route path="/" exact={true}>
                    <MainPage/>
                </Route>
                <Route path="/new" exact={true}>
                    <NewTrack/>
                </Route>
                <Route path="/track/:id" exact={true}>
                    <TrackPage/>
                </Route>
                <Route>
                    <NotFound/>
                </Route>
            </Switch>
        </div>
    }
}


export default inject('app')(observer(App));
