import {Coordinate, Track} from "../tools/Types";

export class BikeApi {
    private readonly url: string;
    private token = "";

    constructor(url: string) {
        this.token = localStorage.getItem("token") || "";
        this.url = url;
    }

    public setToken(token: string) {
        localStorage.setItem("token", token);
        this.token = token
    }

    public getTrack(id: string) {
        return this.request<Track>(`/track/${id}`);
    }

    public allTracks(page: number, count: number) {
        return this.request<Track[]>(`/tracks/?page=${page}&count=${count}&nameSort=bytime`, undefined, false);
    }

    public byCity(page: number, count: number, town: string) {
        return this.request<Track[]>(`/tracks/bycity/?cityname=${town}&page=${page}&count=${count}`, undefined, false);
    }

    public checkAuth() {
        return this.request(`/user/`);
    }

    public byName(page: number, count: number, name: string) {
        return this.request<Track[]>(`/tracks/byname/?cityname=${name}&page=${page}&count=${count}`, undefined, false);
    }

    public createTrack(name: string, cityName: string, description: string) {
        return this.request<{ id: number }>("/track/create/start", {
            method: "PUT",
            body: JSON.stringify({
                name,
                cityName,
                description
            })
        });
    }

    public addComment(id: number, mycomment: string) {
        return this.request("/comment/", {
            method: "PUT",
            body: JSON.stringify({
                id,
                comment: {
                    text: mycomment
                }
            })
        });
    }

    public uploadCoordinates(id: number, file: File) {
        const formData = new FormData();
        formData.append("file", file, "file");
        return this.request<Coordinate[]>(`/track/${id}/coordinates`, {
                method: "PUT",
                body: formData
            },
            true,
            false);
    }

    public register(login: string, password: string) {
        return this.request("/user/create", {
            method: "PUT",
            body: JSON.stringify({
                login,
                password
            })
        }, false);
    }

    public login(login: string, password: string) {
        return this.request<{ token: string }>("/login", {
            method: "POST",
            body: JSON.stringify({
                username: login,
                password
            })
        }, false);
    }

    private async request<T = void>(path: string, init?: RequestInit, needAuth = true, json = true): Promise<T> {
        const params = init ? init : {} as RequestInit;
        if (!params.headers) {
            params.headers = {}
        }
        if (this.token && needAuth)
            params.headers = {
                ...params.headers,
                "Authorization": `Bearer_${this.token}`,
            }
        if (json && !params.headers!!["Content-Type"]) {
            params.headers = {
                ...params.headers,
                "Content-Type": `application/json`
            }
        } else {
            delete params.headers!!['Content-Type'];
        }

        params.mode = "cors";

        const resp = await fetch(`${this.url}${path}`, init);

        if (!resp.ok)
            throw new Error(resp.status + " " + resp.statusText)

        const body = await resp.text();
        if (!body)
            return undefined as unknown as T;

        const jsonAns = JSON.parse(body)

        return jsonAns as T;
    }
}
