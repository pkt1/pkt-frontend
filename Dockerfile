FROM node:12 AS build-stage

ARG REACT_APP_API_URL=http://localhost:3000
ENV REACT_APP_API_URL=${REACT_APP_API_URL}

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . ./

RUN npm run build

#TODO
#RUN npm run test

FROM nginx:latest
COPY --from=build-stage /app/build/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf

ENTRYPOINT ["nginx", "-g", "daemon off;"]
